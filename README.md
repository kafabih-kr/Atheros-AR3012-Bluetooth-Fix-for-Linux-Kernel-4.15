# Description

This bluetooth source is from Linux Kernel 4.15 source (Linus Torvald Official Github).

Attention! this is a DKMS module, so it need to install using DKMS.

# Installation
1. Clone or download this repo, if you download it, you must extract first.
2. Add it to DKMS as below

   <code>sudo dkms add ./Atheros-AR3012-Bluetooth-Fix-for-Linux-Kernel-4.15-master/</code>
3. Install it using DKMS as below

   <code>sudo dkms install bluetooth-ath3k-4.15-fix/((version)) --force</code>
4. Reboot or restart your GNU/Linux machine to check are your Atheros AR3012 Bluetooth is functioned.

# Uninstall
1. Uninstall it using DKMS as below

   <code>sudo dkms remove bluetooth-ath3k-4.15-fix/((version)) --all</code>
4. Reboot or restart your GNU/Linux machine.
